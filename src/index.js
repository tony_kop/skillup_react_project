import React from 'react';
import ReactDOM from 'react-dom';
import { composeWithDevTools } from 'redux-devtools-extension';
import App from './App';
/* Далее будем подключать модули самого redux */
import { Provider } from 'react-redux';
import { createStore } from 'redux';
/* Подключаем общий модуль главный reduxer */
import rootReducer from './system/rootReducer';

//import { createStore, applyMiddleware } from 'redux';

/* const store = createStore(rootReducer, composeWithDevTools(
  applyMiddleware(...middleware),
  // other store enhancers if any
)); */

const store = createStore(rootReducer, composeWithDevTools());


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
