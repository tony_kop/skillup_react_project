const initialState = {
  photos: [],
  favorite: [],
}

export default function(state = initialState, action) {
  console.log('Gallery reducer', action);
  switch(action.type) {
    case 'SET_PHOTOS':
    return {
      // вместо старого state, создаем новый
      ...state,
      photos: [...action.photos]
    }
    default: return state; 
  }
}