import React, {Component} from 'react';
import {connect} from 'react-redux';
import ListPhotos from './ListPhotos';
import "./style.scss";
//import { getApiPhotos } from './service';
import { callApiPhotosAction } from './actions';

//const Gallery = (props) => {
class Gallery extends Component {
  
  componentDidMount() {
    this.props.callApiPhotos();
  };
  render() {
    const { photos } = this.props;
    return (
    <div id = "Gallery" className = "page">
      <h1>Gallery</h1>
      <ListPhotos photos={photos} />
    </div>
  )
  }
}

const mapStateToProps = (state) => ({
  photos: state.gallery.photos,
})

const mapDispatchToProps = (dispatch) => ({
  //setPhotos: photos => dispatch(setPhotosActionType(photos)),
  callApiPhotos: () => callApiPhotosAction(dispatch),
})
export default connect(mapStateToProps, mapDispatchToProps)(Gallery);