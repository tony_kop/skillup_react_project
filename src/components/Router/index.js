import React from 'react';
import Home from '../pages/Home';
import Gallery from '../pages/Gallery';
import Contacts from '../pages/Contacts';
import Photo from '../pages/Photo';
import { Switch, Route } from 'react-router-dom';

export default function () {
  return (
    <Switch>
      <Route exact path='/' component={Home} />
      {/* Ключевое слово exact указывает что адрес '/' это отдельный адрес  */}
      {/* Оба /roster и /roster/:number начинаются с /roster */}
      <Route path='/gallery' component={Gallery} />
      <Route path='/contacts' component={Contacts} />
      {/* Динамический параметр в адресную строку перердается через : */}
      <Route path='/photo/:idPhoto' component={Photo} />
    </Switch>
  )
}