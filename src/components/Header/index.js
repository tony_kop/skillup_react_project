import React from 'react';
import { NavLink } from 'react-router-dom';
import './style.scss';

const Header = () => {
  return (
    <div id = "Header">
      <ul>
        <li><NavLink exact to = "/">Home</NavLink></li>
        {/* Ключевое слово exact указывает что адрес '/' это отдельный адрес
            здесь, например, мы его добавляем, чтобы стиль active добавлялся только к активной ссылке */}
        <li><NavLink to = "/gallery">Gallery</NavLink></li>
        <li><NavLink to = "/contacts">Contacts</NavLink></li>
      </ul>
    </div>
  )
}

export default Header;